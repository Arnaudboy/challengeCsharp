using System;

namespace CSharpDiscovery.Quest02
{
    public class DifferenceInMinutes_Exercice
    {
        public static double DifferenceInMinutes(DateTime start, DateTime end)
        {
            TimeSpan diff = end.Subtract(start);
            return (diff.Days*24*60)+(diff.Hours*60)+diff.Minutes;
        }
    }
}