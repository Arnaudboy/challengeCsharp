namespace CSharpDiscovery.Quest04
{
    public class Car : Vehicule
    {
        public string Model
        {get; set;}

        public Car() : base()
        {
            Model = "Unknown";
        }
        public Car(string model, string Brand, string Color, int CurrentSpeed = 0) : base(Brand, Color, CurrentSpeed)
        {
            this.Model = model;
        }
        public override string ToString()
        {
            return $"{this.Color} {this.Brand} {this.Model}";
        }
        public override void Accelerate(int speed)
        {
            int newSpeed = this.CurrentSpeed+speed;
            if(newSpeed<=180)
            {
                this.CurrentSpeed=newSpeed;
            } else {
                this.CurrentSpeed = 180;
            }
        }
        public override void Brake(int BrakePower)
        {
            int newSpeed = this.CurrentSpeed-BrakePower;
            if(newSpeed>=0)
            {
                this.CurrentSpeed=newSpeed;
            }
            else {
                this.CurrentSpeed = 0;
            }
        }
    }
}