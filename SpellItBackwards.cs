using System;
namespace CSharpDiscovery.Quest01
{
    public class SpellItBackwards_Exercice
    {
        public static string SpellItBackward(string str)
        {
            char[] splitedString = str.ToCharArray();
            Array.Reverse(splitedString);
            return new String(splitedString);
        }
    }
}