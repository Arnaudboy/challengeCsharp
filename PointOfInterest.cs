using System;
namespace CSharpDiscovery.Quest03
{
    public class PointOfInterest
    {
        public double Latitude
        {get; set;}
        public double Longitude
        {get; set;}
        public string Name
        {get; set;}
        public static string googleMapsUrlTemplate = "https://www.google.com/maps/place/{0}/@{1},{2},15z/";
        public static string GoogleMapsUrlTemplate{
            get {return googleMapsUrlTemplate;}
        }

        public PointOfInterest() : this("Bordeaux Ynov Campus", 44.854186, -0.5663056)
        {}
        public PointOfInterest(string name, double latitude, double longitude )
        {
            this.Latitude = latitude;
            this.Longitude = longitude;
            this.Name = name;
        }

        public string GetGoogleMapsUrl()
        {
            string newName = this.Name.Trim().Replace(" ", "+");
            return GoogleMapsUrlTemplate.Replace("{0}", newName).Replace("{1}", this.Latitude.ToString()).Replace("{2}", this.Longitude.ToString());
        }

        public override string ToString()
        {
            return $"{this.Name} (Lat={this.Latitude}, Long={this.Longitude})";
        }

        public int GetDistance(PointOfInterest other)
        {
            double latFrom = (Math.PI / 180) * this.Latitude;
            double lonFrom = (Math.PI / 180) * this.Longitude;
            double latTo = (Math.PI / 180) * other.Latitude;
            double lonTo = (Math.PI / 180) * other.Longitude;
            double latDelta = latTo - latFrom;
            double lonDelta = lonTo - lonFrom;
            double angle = 2 * Math.Sin(Math.Sqrt(Math.Pow(Math.Sin(latDelta / 2), 2) +
            Math.Cos(latFrom) * Math.Cos(latTo) * Math.Pow(Math.Sin(lonDelta / 2), 2)));
            return Convert.ToInt32(Math.Ceiling(angle * 6371));
        }

        public static int GetDistance(PointOfInterest p1, PointOfInterest p2)
        {
            double latFrom = (Math.PI / 180) * p1.Latitude;
            double lonFrom = (Math.PI / 180) * p1.Longitude;
            double latTo = (Math.PI / 180) * p2.Latitude;
            double lonTo = (Math.PI / 180) * p2.Longitude;
            double latDelta = latTo - latFrom;
            double lonDelta = lonTo - lonFrom;
            double angle = 2 * Math.Sin(Math.Sqrt(Math.Pow(Math.Sin(latDelta / 2), 2) +
            Math.Cos(latFrom) * Math.Cos(latTo) * Math.Pow(Math.Sin(lonDelta / 2), 2)));
            return Convert.ToInt32(Math.Ceiling(angle * 6371));
        }
    }
}