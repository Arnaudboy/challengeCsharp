using System;
using System.Globalization;
namespace CSharpDiscovery.Quest02
{
    public class FromStringToDateTime_Exercice
    {
        public static DateTime FromStringToDateTime(string dateStr)
        {
            CultureInfo culture = new CultureInfo("fr-FR");    
            DateTime tempDate = Convert.ToDateTime(dateStr, culture);   
            return tempDate;
        }
    }
}