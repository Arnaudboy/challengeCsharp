using System;
namespace CSharpDiscovery.Quest01
{
    public class ConcatEverything_Exercice
    {
        public static string ConcatEverything(params string[] str)
        {
            string output = "";
            foreach(string item in str){
                output += item;
            }
            return output;
        }
    }
}