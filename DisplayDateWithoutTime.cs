using System;
using System.Globalization;
namespace CSharpDiscovery.Quest02
{
    public class DisplayDateWithoutTime_Exercice
    {
        public static string DisplayDateWithoutTime(DateTime date)
        {
            return date.ToString("d", CultureInfo.CreateSpecificCulture("fr-FR"));
        }
    }
}