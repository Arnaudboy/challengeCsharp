using System;
namespace CSharpDiscovery.Quest02
{
    public class FindLastIndex_Exercice
    {
        public static int? FindLastIndex(int[] tab, int a)
        {
           if(tab == null){
               return null;
           } else {
               int output = Array.FindLastIndex(tab, elem => elem == a);
               if(output==-1){
                   return null;
               }
                return output;
           }
        }
    }
}