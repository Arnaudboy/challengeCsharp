namespace CSharpDiscovery.Quest04 
{
    public class Truck : Vehicule
    {
        public int Tonnage
        {get; set;}

        public Truck() : base()
        {
            Tonnage = 0;
        }

        public Truck(int tonnage,string Brand,string Color, int CurrentSpeed = 0) : base(Brand, Color, CurrentSpeed)
        {
            this.Tonnage = tonnage;
        }
        public override string ToString()
        {
            return $"{this.Color} {this.Brand} {this.Tonnage}T Truck";
        }
        public override void Accelerate(int speed)
        {
            int newSpeed = this.CurrentSpeed+speed;
            if(newSpeed<=100)
            {
                this.CurrentSpeed=newSpeed;
            } else {
                this.CurrentSpeed = 100;
            }
        }
        public override void Brake(int BrakePower)
        {
            int newSpeed = this.CurrentSpeed-BrakePower;
            if(newSpeed>=0)
            {
                this.CurrentSpeed=newSpeed;
            }
            else {
                this.CurrentSpeed = 0;
            }
        }
    }
}